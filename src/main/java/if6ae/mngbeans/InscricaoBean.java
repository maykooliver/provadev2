package if6ae.mngbeans;

import if6ae.entity.Inscricao;
import if6ae.jpa.InscricaoJpa;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.EntityManager;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Mayko
 */
@ManagedBean
@SessionScoped
public class InscricaoBean extends PageBean {
    
    public List<Inscricao> getInscricoes() {
        List<Inscricao> inscricoes;
        try {
            InscricaoJpa ctrl = new InscricaoJpa();
            inscricoes  = ctrl.findAll();
        } catch (Exception e) {
            inscricoes = new ArrayList<>(0);
            log("Lista de inscricoes", e);
        }
        return inscricoes;
    }
    
}
